import React, { Component} from 'react'
import SignUp from "./components/sign-up/SignUp";
import SignIn from "./components/sign-in/SignIn";
import Home from "./components/Home/Home";
import * as ReactDOM from "react-dom";
import { BrowserRouter, Routes, Route, NavLink} from "react-router-dom";
import "./App.css";
export default class App extends Component {
  render() {
    const login = localStorage.getItem("isLoggedIn");
    let navLink = (
      <div className="Tab">
        <NavLink to="/sign-in" className="signIn">
          Sign In
        </NavLink>
        <NavLink to="/" className="signUp">
          Sign Up
        </NavLink>
        <NavLink to="/test" className="test">
          Ntest
        </NavLink>
      </div>
    );
    return (
      <div className="container">
        {login === 'true' ? (
          <BrowserRouter>
            <Routes>
              <Route path="/" element={<SignUp/>}></Route>
              <Route path="/sign-in" element={<SignIn/>}></Route>
              <Route path="/home" element={<Home/>}></Route>
            </Routes>
          </BrowserRouter>
        ) : (
          <BrowserRouter>
            {navLink}
            <Routes>
              <Route path="/" element={<SignUp/>}></Route>
              <Route path="/sign-in" element={<SignIn/>}></Route>
              <Route path="/home" element={<Home/>}></Route>
            </Routes>
          </BrowserRouter>
        )}
      </div>
    )
  }
}

// import React, { Component } from 'react'
// import {BrowserRouter, Routes, Route} from "react-router-dom";
// import PageHome from './ui/home/Home';
// import PageAbout from './ui/about/About';
// import PagePost from './ui/post/Post';
// import PageService from './ui/service/Service';
// import PageLogin from './ui/auth/Login';
// import PageForm from './ui/form/Form';
// import TextareaTag from './ui/textara/Textara';
// import PageSignup from './ui/auth/Singup';
// import Navbar from './ui/menu/manu';

// export default class App extends Component {
//   render() {
//     return (
//         <BrowserRouter>
// 			<Navbar />
// 			<Routes>
// 				<Route path="/" element={<PageHome/>} />
// 				<Route path="/form" element={<PageForm/>} />
// 				<Route path="/textarea" element={<TextareaTag/>} />
// 				<Route path="/signup" element={<PageSignup/>} />
// 				<Route path="/about" element={<PageAbout/>} />
// 				<Route path="/post" element={<PagePost/>} />
// 				<Route path="/service" element={<PageService/>} />
// 				<Route path="/login" element={<PageLogin/>} />
// 				<Route path="*" element={<h1>Page Not error</h1>} />
// 			</Routes>
//         </BrowserRouter>
//     )
//   }
// }