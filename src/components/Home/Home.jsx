import React, { Component } from "react";
import { Button } from "reactstrap";
import { Navigate } from "react-router-dom";
import axios from 'axios';
import './style.css';
export default class Home extends Component {
  state = {
    navigate: false,
    dataUser: [],
	  activePage: 15
  };
  handlePageChange(pageNumber) {
    console.log(`active page is ${pageNumber}`);
    this.setState({activePage: pageNumber});
  }
  componentDidMount() {
    const token = localStorage.getItem("api_token");
    axios({
      method: 'GET',
      url: `http://127.0.0.1:8000/api/users?page=`,
      data: {},
      headers: {
        "Authorization" : `Bearer ${token}`
      }
	}).then((response) => { 
    const users = response.data;
		this.setState({ dataUser:  users.data});
	}).catch(response => {
		console.log(response);
	});
  }
  onLogoutHandler = () => {
    localStorage.clear();
    this.setState({
      navigate: true,
    });
  };
  handleClick = (e, data)=>{
    console.log(data);
  }
  render() {
    const user = JSON.parse(localStorage.getItem("userData"));
    const { navigate } = this.state;
    if (navigate) {
      return <Navigate to="/sign-in" push={true} />;
    }
    return (
	<div className="container">
        <h3> HomePage</h3>
        <div className="row">
          <div className="col-xl-9 col-sm-12 col-md-9 text-dark">
            <h5> Welcome, <span style={{color:"red"}}>{user.name}</span> </h5> You have Logged in
            successfully.
          </div>
          <div className="col-xl-3 col-sm-12 col-md-3">
            <Button
              className="btn btn-primary text-right"
              onClick={this.onLogoutHandler}
            >
              Logout
            </Button>
          </div>
        </div>
        <h2>Manager users</h2>
        <table className="table">
          <thead>
            <tr>
				<th>#No</th>
              	<th>Name</th>
              	<th>Gamail</th>
              	<th>Role</th>
			  	<th>Action</th>
            </tr>
          </thead>
          <tbody>
          {this.state.dataUser.map((data, k) =>{
            return(
              <tr key={k+1}>
				<td>{k+1}</td>
                <td>{data.name}</td>
                <td>{data.email}</td>
                <td>Admin</td>
				<td>
					<button onClick={((e) => this.handleClick(e, data.id))} className="btn btn-danger btn-sm">Delete</button> 
				</td>
              </tr>
            )
          })}
          </tbody>
        </table>
      </div>
    );
  }
}