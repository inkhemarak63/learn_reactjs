import React, { Component } from "react";
import axios from "axios";
import { Navigate } from "react-router-dom";
export default class Signin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      msg: "",
      isLoading: "false",
      redirect: "false",
      errMsgEmail: "",
      errMsgPwd: "",
      errMsg: "",
    };
  }
  onChangehandler = (e) => {
    let name = e.target.name;
    let value = e.target.value;
    let data = {};
    data[name] = value;
    this.setState(data);
  };

  onSignInHandler = (e) => {
    this.setState({ isLoading: "true" });
    console.log(this.state);
    axios
      .post("http://127.0.0.1:8000/api/login", {
        email: this.state.email,
        password: this.state.password,
      })
      .then((response) => {
        this.setState({ isLoading: "false" });
        if (response.status === 200) {
            localStorage.setItem("isLoggedIn", "true");
            localStorage.setItem("api_token", response.data.token);
            localStorage.setItem("userData", JSON.stringify(response.data.users));
         	 this.setState({
            	msg: response.data.message,
            	redirect: "true",
          });      
        }
        if (
          response.data.status === "failed" &&
          response.data.success === undefined
        ) {
          this.setState({
            errMsgEmail: response.data.validation_error.email,
            errMsgPwd: response.data.validation_error.password,
          });
          setTimeout(() => {
            this.setState({ errMsgEmail: "", errMsgPwd: "" });
          }, 2000);
        } else if (
          response.data.status === "failed" &&
          response.data.success === false
        ) {
          this.setState({
            errMsg: response.data.message,
          });
          setTimeout(() => {
            this.setState({ errMsg: "" });
          }, 2000);
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };
  render() {
    if (this.state.redirect === "true") {
      return <Navigate to="/home" />;
    }
    const login = localStorage.getItem("isLoggedIn");
    if (login === 'true') {
      return <Navigate to="/home" />;
    }
    const isLoading = this.state.isLoading;
    return (
      <>
        <br />
        <div className="form-group">
          <label>Email</label>
          <input 
            type="email"
            name="email"
            className='form-control'
            placeholder="Enter email"
            value={this.state.email}
            onChange={this.onChangehandler}
          />
          <span className="text-danger">{this.state.msg}</span>
          <span className="text-danger">{this.state.errMsgEmail}</span>
        </div>
   
        <div className="form-group">
          <label>Password</label>
          <input 
            type="password"
            name="password"
            className='form-control'
            placeholder="Enter password"
            value={this.state.password}
            onChange={this.onChangehandler}
          />
        <span>{this.state.errMsgPwd}</span>
      </div>
      <p>{this.state.errMsg}</p>
      <button onClick={this.onSignInHandler}
        className="btn btn-primary text-center mb-4"
        type="submit"
      >
        Sign In
        {isLoading === "true" ? (
          <span
            className="spinner-border spinner-border-sm ml-5"
            role="status"
            aria-hidden="true"
          ></span>
        ) : (
          <span></span>
        )}
      </button>
    </>
    );
  }
}