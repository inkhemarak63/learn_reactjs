import React, { Component} from 'react'
import "./signup.css";
import { Link } from "react-router-dom";
import axios from 'axios';

class Signup extends Component {
	constructor(props) {
		super(props);
		this.state = {
			signupData: {
				name: "",
				email: "",
				password: "",
				password_confirmation:"",
			},
			isLoading: "",
			msg: ""
		};
		// this.onSubmitHandler = this.onSubmitHandler.bind(this)
	}
	onChangehandler = (e, key) => {
		const { signupData } = this.state;
		signupData[e.target.name] = e.target.value;
		this.setState({ signupData });
	}
	onSubmitHandler = (e) => {
		e.preventDefault();
		this.setState({isLoading : 'true'})
		axios({
			method: 'POST',
			url: 'http://127.0.0.1:8000/api/register',
			data: this.state.signupData,
			headers: {
				'Content-Type': 'application/json',
			}
		}).then((response) => { 
			this.setState({isLoading: "false"});
			if (response.data.status === 200) {
				this.setState({
				msg: response.data.message,
				signupData: {
				name: "",
				email: "",
				password: "",
				password_confirmation: "",
				},
			});
			setTimeout(() => {
				this.setState({ msg: "" });
			}, 2000);
			}
			if (response.data.status === "failed") {
				this.setState({ msg: response.data.message });
				setTimeout(() => {
					this.setState({ msg: "មានចំនុចណាមួយមិនត្រឹមត្រូវហើយ! ចូលពិនិត្យឡើងវិញ" });
				}, 2000);
			}
		}).catch(response => {
			console.log(response);
			this.setState({ msg: response.message});
		});
	};
	render() {
		const isLoading = this.state.isLoading;
		return (
			<form onSubmit={this.handleSignUp}>
				<br />
				{this.state.msg? (
					<div className='alert alert-info'>
						{this.state.msg}
					</div>
					) : (
					<span></span>
				)}
			
				<div className="form-group">
					<label>Name</label>
					<input 
						type="text"
						name="name"
						id="name"
						className='form-control'
						placeholder="Enter name"
						value={this.state.signupData.name}
						onChange={this.onChangehandler}
					/>
				</div>
				<div className="form-group">
					<label>Email</label>
					<input
						type="email"
						name="email"
						className='form-control'
						placeholder="Enter email"
						value={this.state.signupData.email}
						onChange={this.onChangehandler}
					/>
				</div>
				<div className="form-group">
					<label>Password</label>
					<input
						type="password"
						name="password" 
						className='form-control'
						placeholder="Enter password"
						value={this.state.signupData.password}
						onChange={this.onChangehandler}
					/>
				</div>
				<div className="form-group">
					<label>Confirm Password</label>
					<input
						type="password"
						name="password_confirmation" 
						className='form-control'
						placeholder="Confirm Password"
						value={this.state.signupData.password_confirmation}
						onChange={this.onChangehandler}
					/>
				</div>
       		
				<button
					className="text-center mb-4 btn btn-primary"
					color="success"
					onClick={this.onSubmitHandler}
				>
					Sign Up
					{isLoading === 'true' ? (
					<span
						className="spinner-border spinner-border-sm ml-5 btn btn-primary"
						role="status"
						aria-hidden="true"
					></span>
					) : (
					<span></span>
					)}
				</button>
				  <Link to="/sign-in" className="ml-5">I'm already member</Link>
			</form>
		)
	  }
}
export default Signup